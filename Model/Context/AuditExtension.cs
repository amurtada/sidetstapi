﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication4.Model
{
    public class AuditExtension
    {
        [StringLength(128, MinimumLength = 2)]
        public string EnteredBy { get; set; }

        [StringLength(128, MinimumLength = 2)]
        public string ModifiedBy { get; set; }

        [StringLength(128, MinimumLength = 2)]
        public string DeletedBy { get; set; }

        public System.DateTime EnteredDate { get; set; }

        public System.DateTime? ModifiedDate { get; set; }

        public System.DateTime? DeletedDate { get; set; }

        [StringLength(128, MinimumLength = 2)]
        public string Status { get; set; }

        public int Modifycount { get; set; } = 0;

        public void AddEntity(string currentUser)
        {
            EnteredBy = currentUser;
            EnteredDate = DateTime.Now;
            Status = Utils.STATUS_Active;
        }

        public void ModifyEntity(string currentUser)
        {
            ModifiedBy = currentUser;
            ModifiedDate = DateTime.Now;
            Modifycount = Modifycount + 1;
        }

        public void RemoveEntity(string currentUser)
        {
            DeletedBy = currentUser;
            DeletedDate = DateTime.Now;
            Status = Utils.STATUS_Deleted;
        }
    }
}