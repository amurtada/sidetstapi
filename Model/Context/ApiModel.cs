﻿using Microsoft.EntityFrameworkCore;
using Npgsql;


namespace WebApplication4.Model
{
    public class ApiModel : DbContext
    {
        //string connection = "Host=localhost;Port=5432;Username=postgres;Password=gre123;Database=ApiModel;";
        //string connection = "Host=my_host;Database=ApiModel;Username=sidetst;Password=zeroMall";
        string connection = Config.ConnectionString;

        public ApiModel(DbContextOptions<ApiModel> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseNpgsql(connection);
        

        public DbSet<OTPData> OTPData { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }

    }
}