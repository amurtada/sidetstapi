﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Model
{
    public class Section
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long ID { get; set; }

        [StringLength(512, MinimumLength = 4)]
        public string Name { get; set; }

        [StringLength(2048)]
        public string Description { get; set; }

        [StringLength(2048)]
        public string Icon { get; set; }
    }
}
