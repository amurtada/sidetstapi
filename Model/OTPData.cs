﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Model
{
    public class OTPData 
    {
        public long ID { get; set; }

        public long PhoneNumber { get; set; }

        public long GeneratedCode { get; set; }

        public DateTime DateCreated { get; set; }

    }
}
