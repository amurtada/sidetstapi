﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication4.Model
{
    public class Category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long ID { get; set; }

        [StringLength(512, MinimumLength = 4)]
        public string Name { get; set; }

        [ForeignKey("Section")]
        public long Section_ID { get; set; }

        public virtual Section Section { get; set; }

        public virtual ICollection<Product> Products { get; set; }


    }
}
