﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication4.Model
{
    public class User 
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public long PhoneNumber { get; set; }

        public int CountryCode { get; set; }

        public string Pword { get; set; }

        public string Email { get; set; }
    }
}
