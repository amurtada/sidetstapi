﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication4.Model
{
    public class Product 
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long ID { get; set; }

        [StringLength(512, MinimumLength = 4)]
        public string Name { get; set; }

        [ForeignKey("Category")]
        public long Category_ID { get; set; }

        [StringLength(2048)]
        public string Note { get; set; }

        [StringLength(2048)]
        public string Icon { get; set; }

        public long Price { get; set; }

        [JsonIgnore]
        public virtual Category Category { get; set; }
    }
}
