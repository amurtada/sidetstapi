﻿using Microsoft.AspNetCore.Http;
using sidetstapi.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sidetstapi
{
    /// <summary>
    /// Written By : Ahmed Murtada a.rahman
    /// 
    /// Date : 2018 / 11 / 21
    ///
    /// this class represent all utilities that is needed 
    /// in this project .
    /// </summary>
    public partial class Utils
    {
        public static string STATUS_Active = "Active";
        public static string STATUS_Deleted = "Deleted";
        public static string STATUS_Ended = "Ended";
    }



    public partial class Utils
    {
        public static long? GenerateCode(int length)
        {
            long? result;
            string numbers = "1234567890";

            string characters = numbers;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            result = Convert.ToInt64(otp);
            return result;
        }

        public static void addOriginHeader(HttpRequest Request)
        {
            Request.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
        }


    }
}
