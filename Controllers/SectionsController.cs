﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using sidetstapi.Model;

namespace WebApplication4.Controllers
{
    [Produces("application/json")]
    [Route("api/Sections")]
    public class SectionsController : Controller
    {
        private ApiModel db;

        public SectionsController(ApiModel db)
        {
            this.db = db;
        }

        [HttpGet]
        public Object Get()
        {
            Utils.addOriginHeader(Request);
            List<Section> data =  db.Sections.ToList();
            //data = new List<Section>();
            return data;
        }

    }
}