﻿
using Microsoft.AspNetCore.Mvc;
using sidetstapi.Model;
using sidetstapi.Services;
using System;

namespace WebApplication4.Controllers
{
    [Produces("application/json")]
    [Route("api/GenerateCode")]
    public class GenerateCodeController : MainController
    {
        private readonly ApiModel db;

        public GenerateCodeController(ApiModel db)
        {
            this.db = db;
        }

        public int Get(long mobile)
        {
            Utils.addOriginHeader(Request);

            long? code = Utils.GenerateCode(6);
            if (code == null) return -2;

            addOTP(mobile, code.Value);

            MessageService msg = new MessageService(code.Value, mobile);
            bool status = msg.sendMessage();

            
            if (status)
                return 1;
            else
                return -1;
        }

        private void addOTP(long PhoneNumber, long Code)
        {
            OTPData model = new OTPData { PhoneNumber = PhoneNumber, GeneratedCode = Code, DateCreated = DateTime.Now };
            db.OTPData.AddAsync(model);
            db.SaveChangesAsync();
        }
    }
}