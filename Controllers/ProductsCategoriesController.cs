﻿

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sidetstapi.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication4.Controllers
{
    [Route("api/[controller]")]
    public class ProductsCategoriesController : Controller
    {
        private ApiModel db;
        public ProductsCategoriesController(ApiModel db)
        {
            this.db = db;
        }

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Category> Get()
        {
            Utils.addOriginHeader(Request);
            List<Category> data = db.Categories.Include(c => c.Products).ToList();
            return data;
        }
    }
}
