﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using sidetstapi.Model;

namespace WebApplication4.Controllers
{
    [Produces("application/json")]
    [Route("api/verifyCode")]
    public class VerifyCodeController : MainController
    {
        private ApiModel db;

        public VerifyCodeController(ApiModel db)
        {
            this.db = db;
        }

        public int verifyCode(long code, long mobile)
        {
            Utils.addOriginHeader(Request);
            bool check = checkCodeMatch(mobile,code);

            if (check)
                return 1;
            else
                return -1;
        }

        private bool checkCodeMatch(long mobile, long code)
        {
            DateTime beforeOneHour = DateTime.Now.AddMinutes(-15);
            OTPData model = db.OTPData.FirstOrDefault(a => a.DateCreated > beforeOneHour && a.PhoneNumber == mobile);
            if (model == null)
                return false;

            if (model.GeneratedCode == code)
                return true;
            else
                return false;

        }
    }
}