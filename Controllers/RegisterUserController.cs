﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using sidetstapi.DTO;
using sidetstapi.Model;

namespace WebApplication4.Controllers
{
    [Produces("application/json")]
    [Route("api/RegisterUser")]
    public class RegisterUserController : Controller
    {
        private ApiModel db;
        
        public RegisterUserController(ApiModel db)
        {
            this.db = db;
        }

        [HttpPost]
        public int Post(AddUserDTO user)
        {
            Utils.addOriginHeader(Request);
            int check =  addUser(user).Result;
            return check;
        }

        private Task<int> addUser(AddUserDTO user)
        {
            User model = mapToUser(user);
            //model.AddEntity(model.Name);
            db.Users.AddAsync(model);
            return db.SaveChangesAsync();
        }

        private User mapToUser(AddUserDTO user)
        {
            return new User
            {
                Name = user.Name,
                Email = user.Email,
                Pword = user.Pword,
                CountryCode = user.CountryCode,
                PhoneNumber = user.PhoneNumber
            };
        }
    }
}